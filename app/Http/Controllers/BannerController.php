<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Redirect;
use App\Banner;
use Session;
class BannerController extends Controller
{
  
  public function index(){
    return view('admin.banner.index');
  }

  public function create(){
    return view('admin.banner.create');
  }

   public function store(Request $request)
    {
        
        $this->validate($request,[
                   'title'=>'required',
                        
                 ]);
        $request->image = "image";

       $insert = Banner::create([
            'title'       =>$request->title,
            'button_text' =>$request->button_text,
            'button_url'  =>$request->button_url,
            'image'       =>$request->image,
        ]);

      if($insert){
          $message = "data inserted";
      }else{

        $message = "Something worng!";
      }
            
            Session::flash('msg', $message);
            return Redirect::back();
    }
     protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'title' => 'required|string|max:255',
            'title' => 'required',
        ]);
    }


}
